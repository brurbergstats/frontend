import { writable } from 'svelte/store';
import { token } from './userstate.js';

const lang = "en_US";
var t = undefined;
token.subscribe((v) => {t = v});

function createTest() {
  const { subscribe, set, update } = writable(null);

  async function get(addr) {
    console.log(t)
    await fetch(addr, {
        headers: {
          'Authorization': "Bearer " + t
        }
      })
      .then(r => r.json())
      .then(data => {
        set(data);
      });
  }

  return {
    subscribe,
    set,
    get
  }
}

function buildDataset(data) {
  if (!data) {
    return  {
      labels: [],
      datasets: []
    }
  }
  var labels = [];
  var values = [];
  for (let el of data) {
    labels.push(el.day)
    values.push(el.logins)
  }
  return {
    labels: labels,
    datasets: [
      {
        values: values
      }
    ]
  }
}

function createDailyLogins() {
  const { subscribe, set, update } = writable(buildDataset(null));

  async function get(addr) {
    console.log(t)
    await fetch(addr, {
        headers: {
          'Authorization': "Bearer " + t
        }
      })
      .then(r => r.json())
      .then(data => {
        set(buildDataset(data));
      });
  }

  return {
    subscribe,
    set,
    get
  }
}

export const loginsPerDay = createDailyLogins();

window.onload = (event) => {
  loginsPerDay.get(`https://stats.brurberg.no/api/dailylogins`)
};
